package at.journal.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;



@Entity
public class Entry {
	@Id
	@GeneratedValue
	private int id;
	
	@NotNull
	private String text;
	
	 @ManyToOne
	 private User user;

	 
	 public Entry(int id, String text, User user) {
		 super();
		 this.id = id;
		 this.text = text;
		 this.user = user;
	 }

	 public Entry() {
		 super();
	 }	 
	 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void delete(int id) {
		//id.remove();
	}
}
