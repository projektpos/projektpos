package at.journal.db;

import java.util.List;


public interface UserService{
	Iterable<User> findAll();
	
	User findByUsername(String uname);

	User save(User entity);

	User find(int id);
}

	