package at.journal.db;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends  CrudRepository<User, Integer>{
	

	List<User> findByUsername(String name);
	
	
	
}

	