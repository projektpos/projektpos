package at.journal.db;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class User {
	@Id
	@GeneratedValue
	private int id;


	@NotEmpty(message = "{validation.username.NotEmpty.message}")
	@Size(min = 2, max = 60, message = "{validation.username.Size.message}")
	public String username;

	@NotNull
	private String password;
	

	@SuppressWarnings("unused")	// Needed by JPA/Hibernate
	private User() {
	}

	public User(String username, String password) {
		setUsername(username);
		setPassword(password);
	}

	public User(String username, String password, LocalDate reg_date) {
		setUsername(username);
		setPassword(password);
	}

	public String getusername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password="
				+ password + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
}
