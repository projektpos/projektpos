package at.journal;


import java.util.Date;
import java.util.Spliterator;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import at.journal.db.Entry;
import at.journal.db.User;
import at.journal.db.UserService;


@Controller
public class UserController {
	 
	 private final UserService userService;
	 private Entry entries;

	 @Autowired
	 public UserController(UserService userService) {
		 this.userService = userService;
	 }
	 
	 @RequestMapping(value="/home", method=RequestMethod.GET)
	 public void showHome(){
		 
	 }
	 @RequestMapping(value="/login", method=RequestMethod.GET)
	 public void showLogin(){
		 
	 }
	 @RequestMapping(value="/register", method=RequestMethod.GET)
	 public void showReg(){
		 
	 }
	 
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@RequestParam(value = "username", required = true) String username, @RequestParam(value = "password", required = true) String password) {
		User user;
		try{
			if((user = userService.findByUsername(username)) != null && user.getPassword().equals(password) && user.getPassword() != null){
				return "redirect:/home";
			}
		}catch(IndexOutOfBoundsException ex){
			return "redirect:/login";
		}
			
		return "redirect:/login";
			
	}
	
	@RequestMapping(value="/login/add", method=RequestMethod.POST)
	public String addUser(@RequestParam("username") String username, @RequestParam("password") String password) {
	
		int id = 1;
		for(User s : userService.findAll()){
			id++;
		}
		
		if(username != null && password != null && !username.isEmpty() && !password.isEmpty()) {
			for(User u : userService.findAll()){
				if(u.username == username){
					return null;
				}else{
					User user = new User(username, password);
					userService.save(user);
				}
			}
		}
		
		return "redirect:/login";
	}

}
